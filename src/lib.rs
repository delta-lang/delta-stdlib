extern crate delta_lang;

mod print;
mod debug;
mod types;

use std::boxed::Box;
use delta_lang::*;

pub fn init() -> Package {
	let mut pkg = Package::new(Name::new("stdlib".to_owned()));
	
	let print_fn = ExternalFunction::new(Box::new(print::PRINT_FN));
	pkg.set(Name::new("print".to_owned()), Value::Function(Function::External(print_fn)));

	let debug_fn = ExternalFunction::new(Box::new(debug::DEBUG_STACK_FN));
	pkg.set(Name::new("debugStack".to_owned()), Value::Function(Function::External(debug_fn)));

	let typename_fn = ExternalFunction::new(Box::new(types::TYPENAME_FN));
	pkg.set(Name::new("typename".to_owned()), Value::Function(Function::External(typename_fn)));
	
	pkg
}