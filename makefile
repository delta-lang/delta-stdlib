RUST_DIST = nightly
RUSTC = multirust run $(RUST_DIST)

build:
	$(RUSTC) cargo build

release:
	$(RUSTC) cargo build --release