use delta_lang::*;
use std::io::{self, Write};

// static references to bind functions to the static lifetime - Jon 1/31/16
pub static DEBUG_STACK_FN: &'static InvocationCallback = &debug_stack;

pub fn debug_stack(_: Arguments, frame: &mut StackFrame) -> Result<Value, ScriptError> {
	let stdout = io::stdout();
	let mut handle = stdout.lock();
	for value in frame.iter() {
		let fmt = format!("{:?}\n", value);
		try!(handle.write(fmt.as_bytes()).map_err(|e| {
			ScriptError::from(InternalError::from(e))
		}));
	}
	Ok(Value::Undefined)
}