use delta_lang::*;
use std::io::{self, Write};

// static references to bind functions to the static lifetime - Jon 1/31/16
pub static PRINT_FN: &'static InvocationCallback = &print;

pub fn print(args: Arguments, _: &mut StackFrame) -> Result<Value, ScriptError> {
	let stdout = io::stdout();
	let mut handle = stdout.lock();
	for value in args.iter() {
		let fmt = format!("{}\n", String::from(value.as_str()));
		try!(handle.write(fmt.as_bytes()).map_err(|e| {
			ScriptError::from(InternalError::from(e))
		}));
	}
	Ok(Value::Undefined)
}