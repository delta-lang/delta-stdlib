use delta_lang::*;

// static references to bind functions to the static lifetime - Jon 1/31/16
pub static TYPENAME_FN: &'static InvocationCallback = &typename;

pub fn typename(args: Arguments, _: &mut StackFrame) -> Result<Value, ScriptError> {
	if let Some(arg) = args.iter().nth(0) {
		let typename = Str::from(arg.typename());
		return Ok(Value::from(Lit::from(typename)));
	}
	Ok(Value::Undefined)
}